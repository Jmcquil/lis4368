> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS 4368


## Justin McQuillen

### Assignment 1 # Requirements:

*Includes:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of Java Hello
* Screenshot of localhost:9999
* Git commands with description
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init-- initializes git repository
2. git status-- shows status of files in the index
3. git add-- add file chnaes to working directory
4. git commit-- commits any files added
5. git push-- sends changes to master branch
6. git pull-- fetch and merge changes
7. git branch-- lists existing branches 

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![Hello Screenshot](img/Hello.JPG)

*Screenshot of running Https://localhost:9999:

![Localhost Screenshot](img/Localhost.JPG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Jmcquil/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Jmcquil/myteamquotes/ "My Team Quotes Tutorial")