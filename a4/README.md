

# LIS 4368


## Justin McQuillen

### Assignment 4 # Requirements:

*Includes:*

1. Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdownsyntax(README.mdmust also include screenshots as per above.)
2. Blackboard Links: lis4368 Bitbucket repo

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshots of passed and failed;


#### Assignment 4 Screenshots:

*Screenshot A4 Failed*:

![A4 Failed](img/failed.JPG "A4 Failed")

*Screenshot A4 Passed*:

![A4 Passed](img/passed.JPG "A4 Passed")












