> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


# LIS 4368


## Justin McQuillen

### Assignment 2 # Requirements:

*Includes:*

1. http://localhost:9999/hello(displays directory, needs index.html)
2. http://localhost:9999/hello/HelloHome.html(Rename"HelloHome.html" to "index.html")
3. http://localhost:9999/hello/sayhello(invokes HelloServlet)Note: /sayhello maps to HelloServlet.class(changed web.xml file) 
4. http://localhost:9999/hello/querybook.htmle.http://localhost:9999/hello/sayhi(invokes AnotherHelloServlet)

#### README.md file should include the following items:

* Assessment links(as above), and
* Only *one*screenshotof the query resultsfrom the following link(see screenshot below): http://localhost:9999/hello/querybook.html

#### Assignment 2 Links:

*A2 Hello:*
[A2 Hello Link](http://localhost:9999/hello/ "Hello Link")

*A2 HelloHome.html to index.html:*
[A2 HelloHome](http://localhost:9999/hello/ "HelloHome Link")

*A2 Assignment lis4368:*
[A2 lis4368](http://localhost:9999/lis4368/ "lis 4368")

*A2 Say Hi Link:*
[A2 Say hi link](http://localhost:9999/hello/sayhi "Say hi Link")

*A2 QueryBook Link:*
[A2 QueryBook Link](http://localhost:9999/hello/querybook.html "QueryBook Link")

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello*:

![Hello Screenshot](img/hello.JPG)

*Screenshot of http://localhost:9999/hello/index.html:

![Localhost Screenshot](img/helloi.JPG)

*Screenshot of http://localhost:9999/hello/sayhello:

![Localhost Screenshot](img/sayhello.JPG)

*Screenshot of http://localhost:9999/hello/querybook.html:

![Localhost Screenshot](img/querybook.JPG)

*Screenshot of http://localhost:9999/hello/sayhi:

![Localhost Screenshot](img/sayhi.JPG)

*Screenshot of query results:

![Localhost Screenshot](img/results.JPG)


