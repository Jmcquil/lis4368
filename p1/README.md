

# LIS 4368


## Justin McQuillen

### Project 1 # Requirements:

*Includes:*

1.Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdownsyntax(README.mdmust also include screenshots as per above.)
2.Blackboard Links: lis4368 Bitbucket repo
3.*Note*: the carousel *must*contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills.

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per P1;
* Screenshots of Interface's Running;


#### Project 1 Screenshots:

*Screenshot P1 Splash*:

![P1 Splash](img/splash.JPG "Splash screen for lis 4368")

*Screenshot P1 Error*:

![P1 Failed](img/failed.JPG "Data error")

*Screenshot P1 Correct*:

![P1 Passed](img/correct.JPG "Data entered within parameters")














