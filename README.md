# LIS 4368 Requirements:

*Course work links:*

1. [A1 README.md](a1/README.md "A1 README.md")

    * Distributed Version Control with Git and Bitbucket
    * Development Installations
    * Chapter Questions
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)

2. [A2 README.md](a2/README.md "A2 README.md")

    * Assessment: The following links should properly display(see screenshots below):
    * a. http://localhost:9999/hello(displays directory, needs index.html)
    * b. http://localhost:9999/hello/HelloHome.html(Rename"HelloHome.html" to "index.html")
    * c. http://localhost:9999/hello/sayhello(invokes HelloServlet)Note: /sayhello maps to HelloServlet.class(changed web.xml file)
    * d. http://localhost:9999/hello/querybook.html
    * e. http://localhost:9999/hello/sayhi(invokes AnotherHelloServlet)

3. [A3 README.md](a3/README.md "A3 README.md")

    * Provide Bitbucketread-only access to lis4368repo, includelinks to the repos you created in the above tutorials in README.md, using Markdown syntax(README.mdmust also include links and screenshot(s) as per above.)
    * Blackboard Links: lis4368 Bitbucket repo

4. [A4 README.md](a4/README.md "A4 README.md")
    * Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdownsyntax(README.mdmust also include          screenshots as per above.)
    * Blackboard Links: lis4368 Bitbucket repo

5. [A5 README.md](a5/README.md "A5 README.md")
    * Include links to the other assignment repos you created in README.md, using Markdown syntax (README.md must also include screenshots as per aboveand below.)
    * Blackboard Links: lis4368 Bitbucket repo

6. [P1 README.md](p1/README.md "P1 README.md")

    * Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdownsyntax(README.mdmust also include screenshots as per above.)
    * Blackboard Links: lis4368 Bitbucket repo
    * Note: the carousel *must*contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills.

7. [P2 README.md](p2/README.md "P2 README.md")
    
    * Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdownsyntax (README.md must also include screenshots as per above and below.)
    * Blackboard Links: lis4368 Bitbucket repo

   

