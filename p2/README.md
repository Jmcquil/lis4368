# LIS 4368


## Justin McQuillen

### Project 2 # Requirements:

*Includes:*

#### README.md file should include the following items:

* Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdownsyntax (README.md must also include screenshots as per above and below.)
* Blackboard Links: lis4368 Bitbucket repo


#### Project 2 Screenshots:

*Valid user form entry*:

![Valid user form entry](img/customerform.JPG "CustomerForm")

*Passed Validation*:

![Passed Validation](img/passed.JPG "Thanks")

*Modify Form*:

![Modify Form](img/snipping.JPG "Modify")

*Update Customer*:

![ Update Customer](img/modify.JPG " Update Customer")

*Updated Customer*:

![Updated Customer](img/updated.JPG "Updated Customer")

*Delete Warning*:

![Delete Warning](img/delete.JPG "Delete Warning")

*Database changes*:

![Database changes](img/deleted.JPG "Database changes")