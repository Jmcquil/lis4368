

# LIS 4368


## Justin McQuillen

### Assignment 3 # Requirements:

*Includes:*

1. Provide Bitbucketread-only access to lis4368repo, includelinks to the repos you created in the above tutorials in README.md, using Markdown syntax(README.mdmust also include links and screenshot(s) as per above.)
2. Blackboard Links: lis4368 Bitbucket repo

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1;
* Screenshot of ERD;
* Links to the following files: a.a3.mwb b.a3.sql

#### Assignment 3 Screenshots:

*Screenshot A3 ERD*:

![A3 ERD](img/a3.JPG "ERD based upon A3 Requirements")

#### Assignment 3 Links:

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")











