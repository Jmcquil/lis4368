

# LIS 4368


## Justin McQuillen

### Assignment 5 # Requirements:

*Includes:*

#### README.md file should include the following items:

* Include links to the other assignment repos you created in README.md, using Markdown syntax (README.md must also include screenshots as per aboveand below.)
* Blackboard Links: lis4368 Bitbucket repo


#### Assignment 5 Screenshots:

|*Valid User Form Entry*|Passed Validation:|Associated Database Entry:|
|-----------------|------------------|------------------|
|![Valid User Form Entry Screenshot](img/one.JPG)|![Passed Validation Screenshot](img/two.JPG)|![Associated Database Entry Screenshot](img/three.JPG)












